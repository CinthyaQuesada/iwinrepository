import { TestBed, inject } from '@angular/core/testing';

import { SancionColectivaService } from './sancion-colectiva.service';

describe('SancionColectivaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SancionColectivaService]
    });
  });

  it('should be created', inject([SancionColectivaService], (service: SancionColectivaService) => {
    expect(service).toBeTruthy();
  }));
});

import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Campeonato } from '../Domain/Campeonato.model';
import { Observable } from 'rxjs/Observable';
import { Juego } from '../Domain/juego.model';

@Injectable()
export class SancionColectivaService {

  constructor(private http: Http, @Inject('BASE_URL') public url: string) { }
  public campeonatos: Campeonato[];
  getCampeonatos(): Observable<Campeonato[]> {
    console.log(this.url + "api/campeonato")
    return this.http.get(this.url+"api/campeonato", "").map(response => response.json());



  }
  public getJuegos(idCampeonato: number): Observable<Juego[]> {

   return  this.http.get(this.url + 'api/Juego/' + idCampeonato).map(response => response.json());


  }

  public getResultado(idCampeonato: number): Observable<Juego[]> {

    return this.http.get(this.url + 'api/Juego/' + idCampeonato).map(response => response.json());


  }



}




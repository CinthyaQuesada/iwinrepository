import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarSancionColectivaComponent } from './listar-sancion-colectiva.component';

describe('ListarSancionColectivaComponent', () => {
  let component: ListarSancionColectivaComponent;
  let fixture: ComponentFixture<ListarSancionColectivaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarSancionColectivaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarSancionColectivaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

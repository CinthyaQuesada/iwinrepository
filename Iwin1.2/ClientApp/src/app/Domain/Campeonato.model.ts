


export class Campeonato
    {

      identificador: number;
      nombreCampeonato: String;
      imagenCampeonato: string;
      cantidadGrupos: number;
      fechaInicio: Date;

      constructor(identificador?: number, nombreCampeonato?: String, imagenCampeonato?: string,
        cantidadGrupos?: number,
        fechaInicio?: Date) {
        this.cantidadGrupos = cantidadGrupos;
        this.fechaInicio = fechaInicio;
        this.nombreCampeonato = nombreCampeonato;
        this.identificador = identificador;
      }

 



    }


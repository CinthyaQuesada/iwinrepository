import { Component, OnInit, Input } from '@angular/core';
import { jugadorservice } from '../Service/jugadorservice';
import { SancionColectivaService } from '../Service/sancion-colectiva.service';
import { Juego } from '../Domain/juego.model';

@Component({
  selector: 'app-agregar-sancion-colectiva',
  templateUrl: './agregar-sancion-colectiva.component.html',
  styleUrls: ['./agregar-sancion-colectiva.component.css']
})
export class AgregarSancionColectivaComponent implements OnInit {
  idEquipo: number = 1;
  campeonaatoSelecto: number;
  public campeonatos: Campeonato[];
  public juegos: Juego[];
  public juegosSelectos: boolean;
  public juego: Juego;
  constructor(private SancionColectivaService: SancionColectivaService) {

    this.SancionColectivaService.getCampeonatos().subscribe(data => this.campeonatos = data);

  }

  ngOnInit() {



  }


  seleccionar() {
    this.SancionColectivaService.getJuegos(this.campeonaatoSelecto).subscribe(data => this.juegos = data);;
    this.juegosSelectos = false;

  }
  seleccionaJuego(juego: Juego) {
    this.juego = juego;
    this.juegosSelectos = true;

  }

}
interface Campeonato {
  identificador: number;
  nombreCampeonato: String;
  imagenCampeonato: string;
  cantidadGrupos: number;
  fechaInicio: Date;

}


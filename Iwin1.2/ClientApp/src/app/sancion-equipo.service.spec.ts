import { TestBed, inject } from '@angular/core/testing';

import { SancionEquipoService } from './sancion-equipo.service';

describe('SancionEquipoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SancionEquipoService]
    });
  });

  it('should be created', inject([SancionEquipoService], (service: SancionEquipoService) => {
    expect(service).toBeTruthy();
  }));
});

import { Component, OnInit, Input } from '@angular/core';
import { SancionColectiva } from '../Domain/sancionColectiva.model';
import { Resultado } from '../Domain/resultado.model';

@Component({
  selector: 'app-sancion-equipo',
  templateUrl: './sancion-equipo.component.html',
  styleUrls: ['./sancion-equipo.component.css']
})
export class SancionEquipoComponent implements OnInit {
  @Input() idEquipo: number;

  @Input() idJuego: number;
  public sanciones: SancionColectiva[];
  public resultadoA: Resultado;
  public resultadoB: Resultado;
  constructor() {


  }

  ngOnInit() {
  }



}

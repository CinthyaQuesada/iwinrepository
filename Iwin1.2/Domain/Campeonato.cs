﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iwin1._2.Domain
{
    public class Campeonato
    {
        int identificador;
        String nombreCampeonato;
        Byte imagenCampeonato;
        int cantidadGrupos;
        DateTime fechaInicio;
        string tipo;
        string categoria;
        List<Equipo> equipos;
        List<Juego> juegos;

        public Campeonato()
        {

        }

        public Campeonato(int identificador, string nombreCampeonato, byte imagenCampeonato, int cantidadGrupos, DateTime fechaInicio, string tipo, string categoria, List<Equipo> equipos, List<Juego> juegos)
        {
            this.identificador = identificador;
            this.nombreCampeonato = nombreCampeonato ?? throw new ArgumentNullException(nameof(nombreCampeonato));
            this.imagenCampeonato = imagenCampeonato;
            this.cantidadGrupos = cantidadGrupos;
            this.fechaInicio = fechaInicio;
            this.tipo = tipo ?? throw new ArgumentNullException(nameof(tipo));
            this.categoria = categoria ?? throw new ArgumentNullException(nameof(categoria));
            this.equipos = equipos ?? throw new ArgumentNullException(nameof(equipos));
            this.juegos = juegos ?? throw new ArgumentNullException(nameof(juegos));
        }
    }
}


